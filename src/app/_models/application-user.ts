import { Role } from "./role";

export class ApplicationUser {
  userId: number;
  username: string;
  fullname: string;
  email: string;
  role: Role;
  enable: number;
  locked: number;
  photo?: any;
  image?: string;
  encoding?: string;

  // transient
  selected?: boolean = false;
  surveyor?: boolean = false;
  dispatcher?: boolean = false;
}