import { Task } from "./task";
import { ApplicationUser } from "./application-user";
import { ProjectMember } from "./project-member";

export class Assignment {
  assignmentId: number;
  assignmentName: string;
  status: number;
  task: Task
  user: ApplicationUser;
  createdAt: number;

  member?: ProjectMember;
}