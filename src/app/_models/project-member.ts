import { ApplicationUser } from "./application-user";
import { Project } from "./project";
 
export class ProjectMember {
  id?: number;
  user: ApplicationUser
  project: Project
  isDispatcher?: boolean;
  isSuveyor?: boolean;
  distance: number;
  assigned: boolean;
}