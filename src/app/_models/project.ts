import { ApplicationUser } from "./application-user";
import { Question } from "./question";
import { Task } from "./task";

export class Project {

  projectId: number;
  projectName: string;
  projectDescription?: string;
  numOfTasks?: number;
  users?: ApplicationUser[];
  questionForms?: Question[];
  numOfNewTask?: number;
  tasks: Task[];
  imageRequired: boolean = false;
  encoding?: string;
  icon?: string;
}