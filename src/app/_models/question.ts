import { QuestionItem } from "./questionItem";

export class Question {
  orderNumber: number;
  type: string;
  title: string;
  required: boolean;
  questionItems?: QuestionItem[] = [];
  mode: string;
}