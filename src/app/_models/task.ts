import { Assignment } from "./assignment";
import { ProjectMember } from "./project-member";

export class Task {
  taskId?: number;
  taskName: string;
  taskDescription?: string;
  locationName?: string;
  latitude?: number;
  longitude?: number;
  createdAt?: number;
  dueDate?: Date;
  priority: String;
  projectId: number;
  status: number;

  // transient
  uuid: string;
  assignment?: Assignment[];
  numOfAssignments?: number;
  members?: ProjectMember[];
}