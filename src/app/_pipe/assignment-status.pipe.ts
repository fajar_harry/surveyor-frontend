import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'Assignstatus'})
export class AssignmentStatusPipe implements PipeTransform {  
  transform(value: string, args: string[]): any {
    let ivalue: number = Number(value);
    switch (ivalue) {
      case 0:
        return 'On Assignment';
      case 1:
        return 'On Acknowledgment';
      case 2:
        return 'On The Way';
      case 3:
        return 'On Location';
      case 4:
        return 'Completed';
      default:
        return 'UNKNOWN';
    }
  }
}