import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssignmentStatusPipe } from './assignment-status.pipe';


@NgModule({
  imports:[
    CommonModule
  ],
  declarations: [
    AssignmentStatusPipe
  ],
  exports:[
    AssignmentStatusPipe
  ]
})

export class PipesModule {}
