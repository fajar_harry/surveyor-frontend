import { Injectable } from '@angular/core';
import { ApplicationUser } from "../_models/application-user";
import { HttpRequest, HttpClient, HttpEvent, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { Observable } from "rxjs/Observable";
import { AppSettings } from './app-settings';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

@Injectable()
export class ApplicationUserService {

  private apiUrl = AppSettings.API_ENDPOINT + 'users';
  private apiUrlSimple = AppSettings.API_ENDPOINT + 'simple_users';
  private avatarUrl = AppSettings.API_ENDPOINT + 'users/avatar';

  private options: any;

  constructor(private http: HttpClient) 
  {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    var token = currentUser && currentUser.token;
    this.options = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': `Bearer ${token}`
      }),
      observe: 'response'
    };
  }

  findAll(): Observable<HttpEvent<ApplicationUser[]>>  {
    return this.http.get<ApplicationUser[]>(this.apiUrl, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  findAllSimple(): Observable<HttpEvent<ApplicationUser[]>>  {
    return this.http.get<ApplicationUser[]>(this.apiUrlSimple, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  findById(id: number): Observable<HttpEvent<ApplicationUser>> {
    var url = this.apiUrl + "/" + id;
    return this.http.get<ApplicationUser>(url, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  findByUsername(username: string): Observable<HttpEvent<ApplicationUser>> {
    var url = this.apiUrl + "/fetch/" + username;
    return this.http.get<ApplicationUser>(url, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  saveApplicationUser(applicationUser: ApplicationUser): Observable<HttpEvent<ApplicationUser>> {
    return this.http.post(this.apiUrl, JSON.stringify(applicationUser), this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  deleteApplicationUserById(id: number): Observable<HttpEvent<boolean>> {
    var url = this.apiUrl + "/" + id;
    return this.http.delete(url, this.options)
            .pipe(
              catchError(this.handleError)
            );
  }

  updateApplicationUser(applicationUser: ApplicationUser): Observable<HttpEvent<ApplicationUser>> {
    var url = this.apiUrl + "/" + applicationUser.userId;
    return this.http.put(url, applicationUser, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  changeUserActiveStatus(userId: number, status:number): Observable<HttpEvent<boolean>> {
    var state: string = status == 1 ? 'activate' : 'deactivate';
    var url = `${this.apiUrl}/${state}/${userId}`;
    return this.http.post(url, {} , this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  changeUserLockStatus(userId: number, status:number): Observable<HttpEvent<boolean>> {
    var state: string = status == 1 ? 'unlock' : 'lock';
    var url = `${this.apiUrl}/${state}/${userId}`;
    return this.http.post(url, {} , this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  resetUserPassword(userId: number): Observable<HttpEvent<boolean>> {
    var url = `${this.apiUrl}/reset/${userId}`;
    return this.http.post(url, {}, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  changeUserPassword(username: string, oldPassword: string, newPassword: string): Observable<HttpEvent<boolean>> {
    var url = `${this.apiUrl}/change-password/${username}`;
    var body = {"oldPassword": oldPassword, "newPassword" : newPassword}
    return this.http.post(url, JSON.stringify(body), this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  saveAvatar(userid: number, file: File): Observable<HttpEvent<{}>> {
    let formdata: FormData = new FormData();
 
    formdata.append('id', userid.toString()); 
    formdata.append('file', file);
 
    let headers: HttpHeaders = new HttpHeaders(
      {'Authorization': this.options.headers.get('Authorization')}
    );
    
    const req = new HttpRequest('POST', this.avatarUrl, formdata, {
      headers: headers,
      reportProgress: true,
      responseType: 'text'
    });
 
    return this.http.request(req);
  }

  
  loadAvatar(userid: number): Observable<HttpEvent<Blob>> {
    var url = this.avatarUrl + "/" + userid;
    return this.http.get(url, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  private handleError(error: HttpErrorResponse) {
    
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      if (error.status == 500) {
        let exception: any = error.error;
        if (exception['exception'] == 'io.jsonwebtoken.ExpiredJwtException') {
          localStorage.removeItem('currentUser');
          return new ErrorObservable('Token_Expired');
        } else if (exception['exception'] == 'org.springframework.dao.DataIntegrityViolationException') {
          return new ErrorObservable('User sudah di assign, tidak bisa dihapus');
        }
      }
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  };
  

}