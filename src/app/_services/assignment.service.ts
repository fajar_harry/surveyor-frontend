import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from "@angular/http";
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { Observable } from "rxjs/Observable";
import { Assignment } from '../_models/assignment';
import { ApplicationUser } from '../_models/application-user';
import { ProjectMember } from '../_models/project-member';
import { AppSettings } from './app-settings';

@Injectable()
export class AssignmentService {

  private apiUrl = AppSettings.API_ENDPOINT + 'assignments';
  private projectApiUrl = AppSettings.API_ENDPOINT + 'projects';
  private taskApiUrl = AppSettings.API_ENDPOINT + 'tasks';

  private options: RequestOptions;

  constructor(private http: Http) {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    var token = currentUser && currentUser.token;
    var headers = new Headers({'Content-Type': 'application/json'});  
    headers.append('Authorization',`Bearer ${token}`)
    this.options = new RequestOptions({headers: headers});
  }

  findAllAssignment(): Observable<Assignment[]> {
    return this.http.get(this.apiUrl, this.options)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  findByTask(projectId: number, taskId: number): Observable<Assignment[]> {
    var url = this.projectApiUrl + "/" + projectId + "/tasks/" + taskId + "/assignments";
    return this.http.get(url, this.options)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  findSurveyor(taskId: number): Observable<ApplicationUser[]> {
    var url = this.taskApiUrl + "/" + taskId + "/surveyors";
    return this.http.get(url, this.options)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  addSurveyor(taskId: number, users: ApplicationUser[]): Observable<boolean> {
    var url = this.taskApiUrl + "/" + taskId + "/surveyors";
    return this.http.post(url, JSON.stringify(users),this.options)
    .map((res: Response) => res.status === 200)
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  deleteById(taskId: number, assignmentId: number): Observable<boolean> {
    var url = this.taskApiUrl + "/" + taskId + "/assignments/" + assignmentId;
    return this.http.delete(url, this.options)
    .map((res:Response) => res.status === 200)
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
}