import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { AppSettings } from './app-settings';

@Injectable()
export class AuthenticationService {
    public token: string;

    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    login(username: string, password: string): Observable<Boolean> {
        return this.http.post(AppSettings.LOGIN_ENDPOINT + 'login', 
                                JSON.stringify({ username: username, password: password }))
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                console.log(response);
                let token = response.json() && response.json().token;
                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    logout(): void {
        let token = this.token;
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');

        if (token) {
            let headers = new Headers({'Content-Type': 'application/json'});  
            headers.append('Authorization',`Bearer ${token}`)
            let options = new RequestOptions({headers: headers});
            
            this.http.post(AppSettings.LOGIN_ENDPOINT + 'logout', {}, options)
                .subscribe(response => {
                    console.log("logout");
                })
        }
    }
}