import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { Observable } from "rxjs/Observable";
import { ProjectMember } from '../_models/project-member';
import { AppSettings } from './app-settings';
import { HttpErrorResponse, HttpHeaders, HttpEvent, HttpClient } from '@angular/common/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { retry, catchError } from 'rxjs/operators';

@Injectable()
export class ProjectMemberService {

  private apiUrl = AppSettings.API_ENDPOINT + 'members';
  private apiProjectUrl = AppSettings.API_ENDPOINT + 'members-project';
  private assignUrl = AppSettings.API_ENDPOINT + 'assign';

  private options: any;

  constructor(private http: HttpClient) {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    var token = currentUser && currentUser.token;
    this.options = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': `Bearer ${token}`
      }),
      observe: 'response'
    };
  }

  findAllByTask(taskId: number): Observable<HttpEvent<ProjectMember[]>>  {
    let url: string = this.apiUrl + "?task=" + taskId;
    return this.http.get<ProjectMember[]>(url, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  findAllByProject(projectId: number, uuidParam?: string): Observable<HttpEvent<ProjectMember[]>>  {
    let uuid = uuidParam ? '?uuid=' + uuidParam : '';
    let url: string = this.apiProjectUrl + "/" + projectId + uuid;
    return this.http.get<ProjectMember[]>(url, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  assignMember(taskId: number, members: ProjectMember[]): Observable<HttpEvent<boolean>> {
    var url = this.assignUrl + "?task=" + taskId;
    return this.http.post(url, members, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      if (error.status == 500) {
        let exception: any = error.error;
        if (exception['exception'] == 'io.jsonwebtoken.ExpiredJwtException') {
          localStorage.removeItem('currentUser');
          return new ErrorObservable('Token_Expired');
        }
      }
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  };

}