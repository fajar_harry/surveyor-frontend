import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpEvent } from "@angular/common/http";
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { retry, catchError } from 'rxjs/operators';
import { Observable } from "rxjs/Observable";
import { ErrorObservable } from "rxjs/observable/ErrorObservable";
import { Project } from '../_models/project';
import { AppSettings } from './app-settings';
import { Router } from '@angular/router';
import { ApplicationUser } from '../_models/application-user';

@Injectable()
export class ProjectService {

  private apiUrl = AppSettings.API_ENDPOINT + 'projects';
  private projectMemberUrl = AppSettings.API_ENDPOINT + 'project_members';

  private options: any;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    var token = currentUser && currentUser.token;
    
    this.options = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': `Bearer ${token}`
      }),
      observe: 'response'
    };
  }

  findAll(): Observable<HttpEvent<Project[]>>  {
    return this.http.get<Project[]>(this.apiUrl, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  findById(id: number): Observable<HttpEvent<Project>> {
    var url = this.apiUrl + "/" + id;
    return this.http.get<Project>(url, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  findMembers(id: number): Observable<HttpEvent<ApplicationUser[]>> {
    var url = this.projectMemberUrl + "/" + id;
    return this.http.get<ApplicationUser[]>(url, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  saveProject(project: Project): Observable<HttpEvent<Project>> {
    return this.http.post<Project>(this.apiUrl, JSON.stringify(project), this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  updateProject(project: Project): Observable<HttpEvent<Project>> {
    var url = this.apiUrl + "/" + project.projectId;
    return this.http.put<Project>(url, project, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  deleteProjectById(id: number): Observable<HttpEvent<number>> {
    var url = this.apiUrl + "/" + id;
    return this.http.delete<number>(url, this.options)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      if (error.status == 500) {
        let exception: any = error.error;
        if (exception['exception'] == 'io.jsonwebtoken.ExpiredJwtException') {
          localStorage.removeItem('currentUser');
          return new ErrorObservable('Token_Expired');
        } else if (exception['exception'] == 'com.surveyor.backend.exception.UnsupportedEditOperationException') {
          return new ErrorObservable(error.error);
        }
      }
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  };
  

}