import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from "@angular/http";
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { Observable } from "rxjs/Observable";
import { Task } from "../_models/task";
import { ImageTask } from "../_models/image-task";
import { AppSettings } from './app-settings';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpEvent } from '@angular/common/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { retry, catchError } from 'rxjs/operators';

@Injectable()
export class TaskService {

  private apiUrl = AppSettings.API_ENDPOINT + 'tasks';
  private apiWizardUrl = AppSettings.API_ENDPOINT + 'tasks_assign';
  private apiMobileUrl = AppSettings.API_ENDPOINT + 'newMTasks';
  private apiImageUrl = AppSettings.API_ENDPOINT + "mtask/images/"
  private projectApiUrl = AppSettings.API_ENDPOINT + 'projects';

  private options: RequestOptions;
  private clientOptions: any;

  constructor(
        private http: Http,
        private httpClient: HttpClient) {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    var token = currentUser && currentUser.token;
    var headersString = new Headers({'Content-Type': 'application/json'});  
    headersString.append('Authorization',`Bearer ${token}`)
    this.options = new RequestOptions({headers: headersString});
    this.clientOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': `Bearer ${token}`
      }),
      observe: 'response'
    };
  }

  findAll(): Observable<Task[]>  {
    return this.http.get(this.apiUrl, this.options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  findById(id: number): Observable<Task> {
    var url = this.apiUrl + "/" + id;
    return this.http.get(url, this.options)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  findNewMobileTask(): Observable<Task[]>  {
    return this.http.get(this.apiMobileUrl, this.options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getImages(taskId: number): Observable<ImageTask[]> {
    return this.http.get(this.apiImageUrl + taskId, this.options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  findByProject(projectId: number): Observable<Task[]> {
    var url = this.apiUrl + "?project=" + projectId;
    return this.http.get(url, this.options)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  saveTask(task: Task): Observable<Task> {
    return this.http.post(this.apiUrl, JSON.stringify(task), this.options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  saveTaskWizard(task: Task): Observable<HttpEvent<Task>> {
    return this.httpClient.post(this.apiWizardUrl, JSON.stringify(task), this.clientOptions)
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  updateTask(task: Task): Observable<Task> {
    var url = this.apiUrl + "/" + task.taskId;
    return this.http.put(url, task, this.options)
    .map((res:Response) => res.json)
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  deleteTaskById(id: number): Observable<boolean> {
    var url = this.apiUrl + "/" + id;
    return this.http.delete(url, this.options)
    .map((res:Response) => res.status === 200)
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      if (error.status == 500) {
        let exception: any = error.error;
        if (exception['exception'] == 'io.jsonwebtoken.ExpiredJwtException') {
          localStorage.removeItem('currentUser');
          return new ErrorObservable('Token_Expired');
        }
      }
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  };

}