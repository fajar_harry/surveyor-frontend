import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_guards/auth.guards';

export const AppRoutes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
      path: '',
      redirectTo: 'projects',
      pathMatch: 'full',
    }, {
      path: '',
      component: AdminLayoutComponent,
      canActivate: [AuthGuard],
      children: [
    {
        path: '',
        loadChildren: './projects/project.module#ProjectModule'
    }, {
        path: 'profiles',
        loadChildren: './profiles/profiles.module#ProfilesModule'
    }, {
        path: 'components',
        loadChildren: './components/components.module#ComponentsModule'
    }, {
        path: 'forms',
        loadChildren: './forms/forms.module#Forms'
    }, {
        path: 'tables',
        loadChildren: './tables/tables.module#TablesModule'
    }, {
        path: 'widgets',
        loadChildren: './widgets/widgets.module#WidgetsModule'
    }, {
        path: 'users',
        loadChildren: './users/users.module#UsersModule'
    }, {
        path: 'wizard',
        loadChildren: './wizard/wizard.module#WizardModule'
    }, {
        path: 'tasks',
        loadChildren: './task/task.module#TaskModule'
    }, {
        path: 'projects',
        loadChildren: './projects/project.module#ProjectModule'
    }, {
        path: 'assignments',
        loadChildren: './assignments/assignments.module#AssignmentsModule'
    }, {
        path: 'charts',
        loadChildren: './charts/charts.module#ChartsModule'
    }, {
        path: 'calendar',
        loadChildren: './calendar/calendar.module#CalendarModule'
    }, {
        path: '',
        loadChildren: './userpage/user.module#UserModule'
    }, {
        path: '',
        loadChildren: './timeline/timeline.module#TimelineModule'
    }
  ]}, {
      path: '',
      component: AuthLayoutComponent,
      children: [{
        path: 'pages',
        loadChildren: './pages/pages.module#PagesModule'
      }]
    }
];
