import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';

import { Subject } from 'rxjs/Subject';
import { Assignment } from '../_models/assignment';
import { AssignmentService } from '../_services/assignment.service';

declare const $: any;

@Component({
  selector: 'app-assignment-list',
  templateUrl: './assignment-list.component.html'
})
export class AssignmentListComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  assignments: Assignment[] = [];

  sub: any;

  headerRow: string[] = ['ID', 'Surveyor Name', 'Email', 'Task Name', 'Status', 'Creation Date', 'Actions'];

  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private assignmentService: AssignmentService
  ) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
      language: {
        search: '_INPUT_',
        searchPlaceholder: 'Search records',
      }
    };

    this.sub = this.route.params.subscribe(params => {
      this.assignmentService.findAllAssignment()
      .subscribe(
      assignments => {
        this.assignments = assignments;
        this.dtTrigger.next();
      });
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.assignmentService.findAllAssignment()
        .subscribe(
        assignments => {
          this.assignments = assignments;
          this.dtTrigger.next();
        });
    });
  }

  onDelete(taskId: number, assignmentId: number): void {
    this.assignmentService.deleteById(taskId, assignmentId)
      .subscribe(
      result => {
        if (result === true)
          this.rerender();
        else
          console.log('false');
      }
      )
  }
}