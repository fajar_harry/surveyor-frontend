import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { DataTablesModule } from 'angular-datatables';
import { AssignmentRoutes } from './assignments.routing';
import { AssignmentListComponent } from './assignment-list.component';
import { PipesModule } from '../_pipe/pipes.module';
import { AssignmentService } from '../_services/assignment.service';

@NgModule({
    imports: [
        RouterModule.forChild(AssignmentRoutes),
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        DataTablesModule,
        PipesModule
    ],
    declarations: [
        AssignmentListComponent
    ],
    providers: [
        AssignmentService
    ]
})

export class AssignmentsModule {}
