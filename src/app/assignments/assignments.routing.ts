import { Routes } from '@angular/router';
import { AssignmentListComponent } from './assignment-list.component';


export const AssignmentRoutes: Routes = [
    {
      path: '',
      children: [ 
          { path: '', component: AssignmentListComponent}
      ]
}
];
