import { Component, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import { AuthenticationService } from '../_services/authentication.service';


declare interface ValidatorFn {
    (c: AbstractControl): {
        [key: string]: any;
    };
}
declare var $: any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
    private sidebarVisible: boolean;
    private nativeElement: Node;

    login: FormGroup;
    loading = false;
    model: any = {};
    authErrorMessage: string;

    constructor(
        private element: ElementRef, 
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService) 
    {
        this.nativeElement = element.nativeElement;
    }

    ngOnInit() {
        var navbar : HTMLElement = this.element.nativeElement;

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700);

        this.login = this.formBuilder.group({
            username: ['', Validators.required],
            // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
            password: ['', Validators.required]
         });

         // reset login status
        this.authenticationService.logout();
    }

    isFieldValid(form: FormGroup, field: string) {
        return !form.get(field).valid && form.get(field).touched;
    }

    displayFieldCss(form: FormGroup, field: string) {
        return {
          'has-error': this.isFieldValid(form, field),
          'has-feedback': this.isFieldValid(form, field)
        };
    }

    onLogin() {
        this.loading = true;
        if (this.login.valid) {
            this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(result => {
                if (result === true) {
                    this.router.navigate(['/projects']);
                } else {
                    this.authErrorMessage = 'Incorrect Username or Password';
                    this.loading = false;
                }
            }, (err: HttpErrorResponse) => {
                if (err.error instanceof Error) {
                  // A client-side or network error occurred. Handle it accordingly.
                  console.log('An error occurred:', err.error.message);
                } else if (err.status == 401){
                  this.authErrorMessage = 'Incorrect Username or Password';
                  console.log(err.error);
                } else if (err.status == 403) {
                  this.authErrorMessage = 'Account is disabled or locked, please call system administrator';
                } else {
                  // The backend returned an unsuccessful response code.
                  // The response body may contain clues as to what went wrong,
                  let message = `Backend returned code ${err.status}, body was: ${err.error}`;
                  this.showNotification('top', 'right', message);
                  console.log(message);
                }
                this.loading = false;
              }
            );
        } else {
          this.validateAllFormFields(this.login);
          this.loading = false;
        }
        
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
          console.log(field);
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
            control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
            this.validateAllFormFields(control);
          }
        });
    }

    showNotification(from: any, align: any, alert?: string) {
        const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];

        const color = Math.floor((Math.random() * 6) + 1);

        $.notify({
            icon: 'notifications',
            message: alert
        }, {
            type: type[color],
            timer: 3000,
            placement: {
                from: from,
                align: align
            }
        });
    }
}
