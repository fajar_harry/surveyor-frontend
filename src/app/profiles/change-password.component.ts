import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { ApplicationUserService } from "../_services/application-user.service";

declare const $: any;

@Component ({
  templateUrl: './change-password.component.html'
})
export class ChangePasswordComponent implements OnInit {

  private nativeElement: any;

  passwordForm: FormGroup;
  loading = false;
  id: number;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userService: ApplicationUserService) 
  {}

  ngOnInit(): void {
    this.passwordForm = this.formBuilder.group({
      oldPassword: ['', Validators.required],
      newPassword1: ['', Validators.required],
      newPassword2: ['', Validators.required]
    });
  }

  isRetypeFieldValid(form: FormGroup, field: string) {
    let newPassword = form.get('newPassword1').value;
    let newConfirmPassword = form.get('newPassword2').value;
    return newPassword !== newConfirmPassword;
  }

  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field).valid && form.get(field).touched;
  }

  displayFieldCss(form: FormGroup, field: string) {
    return {
      'has-error': this.isFieldValid(form, field),
      'has-feedback': this.isFieldValid(form, field)
    };
  }

  displayRetypeFieldCss(form: FormGroup, field: string) {
    return {
      'has-error': this.isRetypeFieldValid(form, field),
      'has-feedback': this.isRetypeFieldValid(form, field)
    };
  }

  onSave(): void {
    let newPassword = this.passwordForm.get('newPassword1').value;
    let oldPassword = this.passwordForm.get('oldPassword').value;
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
 
    this.userService.changeUserPassword(currentUser.username, oldPassword, newPassword)
    .subscribe(
      event => {
        if (event.type === HttpEventType.Response) {
          if (event.body === true)
            this.showNotification("Password changed", 'danger');
          else
            this.showNotification("Change password failed", 'danger');
        } 
      }, 
      error => {
        if (error === 'Token_Expired') this.router.navigate(['/login']);
        else this.showNotification(error, 'danger');
      });
  }

  onCancel(): void {
    this.router.navigate(['/admins/employee']);
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      console.log(field);
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  showNotification(alert: string, msgType: string) {
    $.notify({
      icon: 'notifications',
      message: alert
    }, {
        type: msgType,
        timer: 3000,
        placement: {
          from: 'top',
          align: 'right'
        }
      });
  }
}