import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';

import { ProfilesRoutes } from './profiles.routing';
import { ChangePasswordComponent } from './change-password.component';
import { MessagesModule } from '../messages/messages.module';
import { ApplicationUserService } from '../_services/application-user.service';
import { UserProfileComponent } from './user-profile.component';


@NgModule({
  imports:[
    CommonModule,
    RouterModule.forChild(ProfilesRoutes),
    FormsModule,
    ReactiveFormsModule,
    MessagesModule,
    MaterialModule
  ],
  declarations:[
    ChangePasswordComponent,
    UserProfileComponent
  ],
  providers:[
    ApplicationUserService
  ]
})

export class ProfilesModule {}
