import { Routes } from '@angular/router';
import { ChangePasswordComponent } from './change-password.component';
import { UserProfileComponent } from './user-profile.component';

export const ProfilesRoutes: Routes = [
    {
      path: 'changePassword', component: ChangePasswordComponent,
    }, {
      path: 'view', component: UserProfileComponent
    }
];
