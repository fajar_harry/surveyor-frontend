import { Component, OnInit, OnDestroy, AfterViewInit, ElementRef, Renderer2, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { ApplicationUser } from "../_models/application-user";
import { Role } from "../_models/role";
import { ApplicationUserService } from "../_services/application-user.service";


declare const $: any;
interface FileReaderEventTarget extends EventTarget {
  result: string;
}

interface FileReaderEvent extends Event {
  target: FileReaderEventTarget;
  getMessage(): string;
}

@Component({
  templateUrl: './user-profile.component.html',
  providers: [ApplicationUserService]
})
export class UserProfileComponent implements OnInit, AfterViewInit {
  
  private nativeElement: any;

  userForm: FormGroup;
  loading = false;
  model: ApplicationUser;
  id: number;
  formTitle: string;

  roles: Role[] = [
      {"roleId": 1, "roleName": 'Administrator'},
      {"roleId": 2, "roleName": "Dispatcher"},
      {"roleId": 3, "roleName": "Surveyor"}
    ];

  constructor(
    private element: ElementRef,
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userService: ApplicationUserService,
    private dialog: MatDialog) {
    this.nativeElement = element.nativeElement;
  }

  ngAfterViewInit(): void {
    if (this.id)
      this.renderer.selectRootElement('#name').focus();
  }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      username: ['', Validators.required],
      fullname: ['', Validators.required],
      email: ['', Validators.required],
      roleId: []

    });

    let currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.userService.findByUsername(currentUser.username)
    .subscribe(
      event => {
        if (event.type === HttpEventType.Response) {
          let user: ApplicationUser = event.body;
          this.id = user.userId;
          this.userForm.patchValue({
            id: user.userId,
            username: user.username,
            fullname: user.fullname,
            email: user.email,
            roleId: user.role.roleId
          });
        } 
      }, 
      error => {
        if (error === 'Token_Expired') this.router.navigate(['/login']);
        else this.showNotification(error, 'danger');
      });
  }


  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field).valid && form.get(field).touched;
  }

  displayFieldCss(form: FormGroup, field: string) {
    return {
      'has-error': this.isFieldValid(form, field),
      'has-feedback': this.isFieldValid(form, field)
    };
  }

  onSave(): void {
    this.loading = true;
    if (this.userForm.valid) {
      this.model = this.userForm.value;
      this.model.userId = this.id;
      if (this.id) {
        this.userService.updateApplicationUser(this.userForm.value)
          .subscribe(result => {
            this.router.navigate(['users']);
          }, (err: HttpErrorResponse) => {
            console.log("An error occured");
          })
      } else {
        this.userService.saveApplicationUser(this.model)
          .subscribe(result => {
            console.log(result)
            this.router.navigate(['users']);
          }, (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              let message = `Backend returned code ${err.status}, body was: ${err.error}`;
              this.showNotification('top', 'right', message);
              console.log(message);
            }
            this.loading = false;
          });
      }
    } else {
      this.validateAllFormFields(this.userForm);
      this.loading = false;
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      console.log(field);
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  showNotification(from: any, align: any, alert?: string) {
    const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];

    const color = Math.floor((Math.random() * 6) + 1);

    $.notify({
      icon: 'notifications',
      message: alert
    }, {
        type: type[color],
        timer: 3000,
        placement: {
          from: from,
          align: align
        }
      });
  }
}