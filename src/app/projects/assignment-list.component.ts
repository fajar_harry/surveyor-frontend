import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';

import { Subject } from 'rxjs/Subject';
import { Assignment } from '../_models/assignment';
import { AssignmentService } from '../_services/assignment.service';
import {SurveyorLookupDialogComponent } from './surveyor-lookup.dialog';

declare const $: any;

@Component({
  selector: 'app-assignment-list',
  templateUrl: './assignment-list.component.html'
})
export class AssignmentListComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  assignments: Assignment[] = [];

  projectId: number;
  taskId: number;
  sub: any;
  surveyorLookupDialogRef: MatDialogRef<SurveyorLookupDialogComponent>;

  headerRow: string[] = ['ID', 'Surveyor Name', 'Email', 'Distance', 'status', 'Creation Date', 'Actions'];

  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private assignmentService: AssignmentService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
      language: {
        search: '_INPUT_',
        searchPlaceholder: 'Search records',
      }
    };

    this.sub = this.route.params.subscribe(params => {
      this.projectId = params['projectId'];
      this.taskId = params['taskId'];
      this.assignmentService.findByTask(this.projectId, this.taskId)
      .subscribe(
      assignments => {
        this.assignments = assignments;
        this.dtTrigger.next();
      });
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.assignmentService.findByTask(this.projectId, this.taskId)
        .subscribe(
        assignments => {
          this.assignments = assignments;
          this.dtTrigger.next();
        });
    });
  }

  gotoTasks(): void {
    this.router.navigate(['projects', this.projectId, 'tasks']);
  }

  newSurveyor(): void {
    const matDialogConfig = new MatDialogConfig();
      matDialogConfig.width = "860px";
      matDialogConfig.height = "540px";
    this.surveyorLookupDialogRef = this.dialog.open(SurveyorLookupDialogComponent, matDialogConfig);
    this.surveyorLookupDialogRef.afterClosed()
      .subscribe(users => {
        this.assignmentService.addSurveyor(this.taskId, users)
        .subscribe(
        result => {
          if (result === true) {
            this.showNotification('right', 'top', 'Add assignment to surveyor success');
            this.rerender();
          } else
            this.showNotification('right', 'top', 'Add assignment to surveyor failed');
        });
      });
  }

  onDelete(id: number): void {
    this.assignmentService.deleteById(this.taskId, id)
      .subscribe(
      result => {
        if (result === true)
          this.rerender();
        else
          console.log('false');
      }
      )
  }

  showNotification(from: any, align: any, alert?: string) {
    const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];

    const color = Math.floor((Math.random() * 6) + 1);

    $.notify({
      icon: 'notifications',
      message: alert
    }, {
        type: type[color],
        timer: 3000,
        placement: {
          from: from,
          align: align
        }
      });
  }
}