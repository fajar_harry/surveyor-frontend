import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";


@Component({
  selector: 'app-form',
  templateUrl: './form-create.component.html'
})
export class FormCreateComponent implements OnInit {

  typeOfQuestions: string[] =['TextBox', 'TextArea', 'Checkbox', 'Radio'];

  questionForm: FormGroup;
  optionsCb: string[] = ['option 1'];
  optionsRadio: string[] = [];

  constructor(
    private formBuilder: FormBuilder
  ){}

  ngOnInit(): void {
    this.questionForm = this.formBuilder.group({
      type: ['', Validators.required],
      title: ['', Validators.required],
      instruction: [],
      requiredCB: [],
      checkbox1: [],
      radio1: []
    });
  }

  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field).valid && form.get(field).touched;
  }

  displayFieldCss(form: FormGroup, field: string) {
    return {
      'has-error': this.isFieldValid(form, field),
      'has-feedback': this.isFieldValid(form, field)
    };
  }

  currentPlaceholder(): string {
    let type = this.questionForm.get('type').value;
    return type ? '' : 'Select field type';
  }

  onAddOptions(): void {
    let current: number = this.optionsCb.length + 1;
    this.optionsCb.push(`Options ${current}`);
  }

  onAddOthers(): void {
    this.optionsCb.push('Others');
  }

  onDeleteOptions(value: string): void {
    let index = this.optionsCb.indexOf(value);
    this.optionsCb.splice(index, 1);
  }

  isOthers(value: string): boolean{
    return value === 'Others';
  }
}