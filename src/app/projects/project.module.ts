import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from '../md/md.module';
import { MaterialModule } from '../app.module';

import { ProjectRoutes } from './project.routing';
import { ProjectComponent } from './projectlist/project.component';
import { ProjectService } from '../_services/project.service';
import { MessagesModule } from '../messages/messages.module';
import { TaskListComponent } from './task-list.component';
import { TaskService } from '../_services/task.service';
import { DataTablesModule } from 'angular-datatables';
import { TaskCreateComponent } from './task-create.component';
import { GMapModule } from 'primeng/primeng';
import { AssignmentListComponent } from './assignment-list.component';
import { AssignmentService } from '../_services/assignment.service';
import { SurveyorLookupDialogComponent } from './surveyor-lookup.dialog';
import { PipesModule } from 'app/_pipe/pipes.module';
import { FormCreateComponent } from './form-create.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ProjectRoutes),
        FormsModule,
        ReactiveFormsModule,
        MessagesModule,
        DataTablesModule,
        MaterialModule,
        GMapModule,
        PipesModule
    ],
    declarations: [
        ProjectComponent,
        TaskListComponent,
        TaskCreateComponent,
        AssignmentListComponent,
        SurveyorLookupDialogComponent,
        FormCreateComponent
    ],
    providers: [
        ProjectService,
        TaskService, 
        AssignmentService
    ],
    entryComponents: [
        SurveyorLookupDialogComponent
    ],
})

export class ProjectModule {}
