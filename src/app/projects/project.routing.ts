import { Routes } from '@angular/router';
import { ProjectComponent } from './projectlist/project.component';
import { TaskListComponent } from './task-list.component';
import { TaskCreateComponent } from './task-create.component';
import { AssignmentListComponent } from './assignment-list.component';
import { FormCreateComponent } from './form-create.component';


export const ProjectRoutes: Routes = [
    {
      path: '',
      children: [ 
          { path: '', component: ProjectComponent},
          { path: ':projectId/tasks', component: TaskListComponent },
          { path: ':projectId/tasks/create', component: TaskCreateComponent },
          { path: ':projectId/tasks/edit/:id', component: TaskCreateComponent },
          { path: ':projectId/tasks/:taskId/assignments', component: AssignmentListComponent },
          { path: ':projectId/forms', component: FormCreateComponent }
      ]
}
];
