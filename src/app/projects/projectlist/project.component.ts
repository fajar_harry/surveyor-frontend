import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ProjectService } from '../../_services/project.service';
import { Project } from '../../_models/project';
import { Task } from '../../_models/task';
import { Router } from '@angular/router';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { HttpEventType } from '@angular/common/http';

declare const $: any;

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit, AfterViewInit {
 
  colors: string[] = ['purple', 'blue', 'green', 'orange', 'red', 'rose'];
  projects: Project[] = [];

  constructor(
      private projectService: ProjectService,
      private router: Router
  ){}

  public ngOnInit() {
    this.projectService.findAll()
    .subscribe(
      event => {
        if (event.type === HttpEventType.Response) {
          this.projects = event.body;
          console.log(this.projects);
        } 
      }, 
      error => {
        if (error === 'Token_Expired') this.router.navigate(['/login']);
        else this.showNotification(error, 'danger');
      });
  }

  ngAfterViewInit() {}

  getColor(index: number): string {
    return this.colors[index % 6];
  }

  gotoEdit(project: Project): void {
    localStorage.setItem("project", JSON.stringify(project));
    this.router.navigate(['wizard', 'conf', project.projectId]);
  }

  gotoTasks(project: Project): void {
    this.router.navigate(['tasks'], { queryParams: { project: project.projectId, name: project.projectName } });
  }

  createProject(): void {
    this.router.navigate(['wizard']);
  }

  getProjectIcon(project: Project): string {
    if (project.icon)
      return project.encoding + ',' + project.icon;
    else
      return '';
  }

  showNotification(alert: string, msgType: string) {
    $.notify({
      icon: 'notifications',
      message: alert
    }, {
        type: msgType,
        timer: 3000,
        placement: {
          from: 'top',
          align: 'right'
        }
      });
  }
}
