import { Component, Input } from "@angular/core";
import { Subject } from 'rxjs/Subject';
import { MatDialogRef } from "@angular/material";
import { ApplicationUser } from "../_models/application-user";
import { AssignmentService } from "../_services/assignment.service";


@Component({
  templateUrl: './surveyor-lookup.dialog.html'
})
export class SurveyorLookupDialogComponent {

  dtOptions: DataTables.Settings = {};
  users: ApplicationUser[] = [];

  headerRow: string[] = ['', 'ID', 'Name', 'Email'];

  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject();

  selectedData: ApplicationUser[] = [];

  noSelectedMessage: string;

  constructor(
    private assignmentService: AssignmentService,
    private dialogRef: MatDialogRef<SurveyorLookupDialogComponent>
  ) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
      language: {
        search: '_INPUT_',
        searchPlaceholder: 'Search records',
      }
    };

    this.assignmentService.findSurveyor(1)
      .subscribe(
      users => {
        this.users = users;
        this.dtTrigger.next();
      });
  }

  onDialogClose(): void {
    this.dialogRef.close();
  }

  onDialogApprove(): void {
    if (this.selectedData || this.selectedData.length > 0)
      this.dialogRef.close(this.selectedData);
    else
      this.noSelectedMessage = 'Please select one or more surveyor';
  }

  onRowSelected(user: ApplicationUser, element: HTMLInputElement): void {
    if (element.checked)
      this.selectedData.push(user);
    else {
      let index = this.selectedData.indexOf(user);
      this.selectedData.splice(index, 1);
    }
  }
}