import { Component, OnInit, OnDestroy, AfterViewInit, ElementRef, Renderer2, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Task } from '../_models/task';
import { TaskService } from '../_services/task.service';

 
declare const $: any;
declare var google;

@Component({
  templateUrl: './task-create.component.html'
})
export class TaskCreateComponent implements OnInit, OnDestroy, AfterViewInit {
  
  private nativeElement: any;

  center_lat: number = -6.312218;
  center_lng: number = 106.6924753;

  taskForm: FormGroup;
  loading = false;
  model: Task;
  id: number;
  projectId: number;
  sub: any;
  formTitle: string;
  mapOptions: any;
  mapOverlays: any[];

  selectedLat: number;
  selectedLng: number;

  constructor(
    private element: ElementRef,
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private taskService: TaskService
  ) {
    this.nativeElement = element.nativeElement;
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  ngAfterViewInit(): void {
    if (this.id)
      this.renderer.selectRootElement('#name').focus();
  }

  ngOnInit(): void {
    this.taskForm = this.formBuilder.group({
      taskName: ['', Validators.required],
      taskDescription: [],
      locationName: []
    });

    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.projectId = params['projectId'];
    });

    if (this.id) { //edit form
      this.formTitle = 'Edit Task Form';
      this.taskService.findById(this.id).subscribe(
        task => {
          this.id = task.taskId;
          this.taskForm.patchValue({
            taskId: task.taskId,
            taskName: task.taskName,
            taskDescription: task.taskDescription,
            locationName: task.locationName
          });
          this.mapOverlays = []; // clear overlays
          if (!task.latitude || !task.longitude) { 
            task.latitude = this.center_lat;
            task.longitude = this.center_lng;
          }
          
          this.mapOverlays.push(
            new google.maps.Marker({ position: {lat: task.latitude, lng: task.longitude}, draggable: true })
          )
        }, error => {
          console.log(error);
        }
      );
    } else this.formTitle = "New Task Form";

    this.mapOptions = {
      center: {lat: this.center_lat, lng: this.center_lng},
      zoom: 12
    };
    this.mapOverlays = [
      new google.maps.Marker(
        { position: {lat: this.center_lat, lng: this.center_lng}, 
          title: "",
          draggable: true
        }
      )
    ];
  }

  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field).valid && form.get(field).touched;
  }

  displayFieldCss(form: FormGroup, field: string) {
    return {
      'has-error': this.isFieldValid(form, field),
      'has-feedback': this.isFieldValid(form, field)
    };
  }

  onSave(): void {
    this.loading = true;
    if (this.taskForm.valid) {
      this.model = this.taskForm.value;
      this.model.taskId = this.id;
      if (this.id) {
        this.taskService.updateTask(this.model)
          .subscribe(result => {
            this.router.navigate(['/projects', this.projectId, 'tasks']);
          }, (err: HttpErrorResponse) => {
            console.log("An error occured");
          })
      } else {
        /*
        this.model.project = {
          projectId: this.projectId,
          projectName: ''
        };
        */
        this.model.latitude = this.selectedLat ? this.selectedLat : this.center_lng;
        this.model.longitude = this.selectedLng ? this.selectedLng : this.center_lng;

        this.taskService.saveTask(this.model)
          .subscribe(result => {
            console.log(result)
            this.router.navigate(['/projects', this.projectId, 'tasks']);
          }, (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              let message = `Backend returned code ${err.status}, body was: ${err.error}`;
              this.showNotification('top', 'right', message);
              console.log(message);
            }
            this.loading = false;
          });
      }
    } else {
      this.validateAllFormFields(this.taskForm);
      this.loading = false;
    }
  }

  onCancel(): void {
    this.router.navigate(['/projects', this.projectId, 'tasks']);
  }

  onDelete(): void {
    this.taskService.deleteTaskById(this.id)
    .subscribe(
      result => {
        if (result === true)
          this.router.navigate(['/projects', this.projectId, 'tasks']);
        else
          console.log('false');
      }
    )
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      console.log(field);
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  handleDragEnd(event): void {
    this.selectedLat = event.overlay.position.lat();
    this.selectedLng = event.overlay.position.lng();
  }

  showNotification(from: any, align: any, alert?: string) {
    const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];

    const color = Math.floor((Math.random() * 6) + 1);

    $.notify({
      icon: 'notifications',
      message: alert
    }, {
        type: type[color],
        timer: 3000,
        placement: {
          from: from,
          align: align
        }
      });
  }
}