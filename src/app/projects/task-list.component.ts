import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';

import { Subject } from 'rxjs/Subject';
import { TaskService } from '../_services/task.service';
import { Task } from '../_models/task';

declare const $: any;

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html'
})
export class TaskListComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  tasks: Task[] = [];

  projectId: number;
  sub: any;

  headerRow: string[] = ['ID', 'Task Name', 'Task Description', 'Location', 'Creation Date', 'Actions'];

  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private taskService: TaskService
  ) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
      language: {
        search: '_INPUT_',
        searchPlaceholder: 'Search records',
      }
    };

    this.sub = this.route.params.subscribe(params => {
      this.projectId = params['projectId'];
      console.log(this.projectId);
      this.taskService.findByProject(this.projectId)
      .subscribe(
      tasks => {
        this.tasks = tasks;
        this.dtTrigger.next();
      });
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.taskService.findByProject(this.projectId)
        .subscribe(
        tasks => {
          this.tasks = tasks;
          this.dtTrigger.next();
        });
    });
  }

  gotoProjects(): void {
    this.router.navigate(['projects']);
  }

  newTask(): void {
    this.router.navigate(['projects', this.projectId, 'tasks', 'create']);
  }

  onDelete(id: number): void {
    this.taskService.deleteTaskById(id)
      .subscribe(
      result => {
        if (result === true)
          this.rerender();
        else
          console.log('false');
      }
      )
  }

  onEdit(id: number): void {
    this.router.navigate(['projects', this.projectId, 'tasks', 'edit', id]);
  }

  onAssignment(id: number):void {
    this.router.navigate(['projects', this.projectId, 'tasks', id, 'assignments']);
  }

  showNotification(from: any, align: any, alert?: string) {
    const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];

    const color = Math.floor((Math.random() * 6) + 1);

    $.notify({
      icon: 'notifications',
      message: alert
    }, {
        type: type[color],
        timer: 3000,
        placement: {
          from: from,
          align: align
        }
      });
  }
}