import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from './navbar.component';
import { TaskService } from 'app/_services/task.service';

@NgModule({
    imports: [ RouterModule, CommonModule ],
    declarations: [ NavbarComponent ],
    providers: [ TaskService ],
    exports: [ NavbarComponent ]
})

export class NavbarModule {}
