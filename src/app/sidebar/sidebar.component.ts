import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import { AuthenticationService} from '../_services/authentication.service';
import { Router } from '@angular/router';
import { HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { ApplicationUser } from '../_models/application-user';
import { ApplicationUserService } from '../_services/application-user.service';

declare const $: any;

//Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [
    {
        path: '/users',
        title: 'Application Users',
        type: 'link',
        icontype: 'supervisor_account'

    }, {
        path: '/projects',
        title: 'Kategori',
        type: 'link',
        icontype: 'business_center'

    }, {
        path: '/assignments',
        title: 'Laporan',
        type: 'link',
        icontype: 'build'

    }
];
@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];

    user: ApplicationUser;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: ApplicationUserService
    ) {}

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        // fetch user details
        let json = localStorage.getItem('currentUser');
        let username = JSON.parse(json).username;
        this.userService.findByUsername(username)
        .subscribe(
            event => {
                if (event.type === HttpEventType.Response) {
                    this.user = event.body;
                    $('#profilePreview').attr('src', `${this.user.encoding},${this.user.image}`).fadeIn('slow'); 
                } 
              }, 
              error => {
                if (error === 'Token_Expired') this.router.navigate(['/login']);
                else this.showNotification(error, 'danger');
              });
    }
    updatePS(): void  {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            let ps = new PerfectScrollbar(elemSidebar, { wheelSpeed: 2, suppressScrollX: true });
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }
    onLogout() {
        console.log("on logout");
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }
    showNotification(alert: string, msgType: string) {
        $.notify({
          icon: 'notifications',
          message: alert
        }, {
            type: msgType,
            timer: 3000,
            placement: {
              from: 'top',
              align: 'right'
            }
          });
      }
}
