import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Subject';

import { ProjectMember } from '../../_models/project-member';
import { ProjectMemberService } from '../../_services/project-member.service';
import { HttpEventType } from '@angular/common/http';

declare const $: any;

@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.component.html'
})
export class AssignmentComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};

  dtTrigger: Subject<any> = new Subject();

  private taskId: number;
  private projectId: number;
  private members: ProjectMember[] = [];



  constructor(
    private memberService: ProjectMemberService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  public ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
      language: {
        search: '_INPUT_',
        searchPlaceholder: 'Search records',
      }
    };

    this.route
      .queryParams
      .subscribe(params => {
        this.taskId = params['task'];
        this.projectId = params['project'];
        this.memberService.findAllByTask(this.taskId)
          .subscribe(
            event => {
              if (event.type === HttpEventType.Response) {
                this.members = event.body;
              }
            });
        this.dtTrigger.next();
      });
  }

  selectUnselectAll(event): void {
    let flag: boolean = event.target.checked;
    this.members.forEach(element => element.assigned = flag);
  }

  onCancel(): void {
    this.router.navigate(['tasks'], { queryParams: { project: this.projectId } });
  }

  onSave(): void {
    let parameters: ProjectMember[] = [];
    this.members.forEach(element => {
      if (element.assigned) parameters.push(element);
    })

    this.memberService.assignMember(this.taskId, parameters)
      .subscribe(event => {
        if (event.type === HttpEventType.Response) {
          console.log(event.body);
          if (event.body)
            this.router.navigate(['tasks'], { queryParams: { project: this.projectId } });
          else
            this.showNotification('Assign members failed, failed called backend service', 'danger');
        }
      });
  }

  showNotification(alert: string, msgType: string) {
    $.notify({
      icon: 'notifications',
      message: alert
    }, {
        type: msgType,
        timer: 3000,
        placement: {
          from: 'top',
          align: 'right'
        }
      });
  }

}
