import { Component, Input } from "@angular/core";
import { Subject } from 'rxjs/Subject';
import { MatDialogRef } from "@angular/material";
import { SafeResourceUrl } from "@angular/platform-browser";


@Component({
  templateUrl: './image.dialog.html',
})
export class ImageShowDialogComponent {

  theImage: SafeResourceUrl;

  constructor(
    private dialogRef: MatDialogRef<ImageShowDialogComponent>
  ) { }

  ngOnInit(): void {}

  onDialogClose(): void {
    this.dialogRef.close();
  }

}