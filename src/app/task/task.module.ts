import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { FieldErrorDisplayComponent } from '../messages/field-error-display.component';
import { MessagesModule } from '../messages/messages.module';
import { TaskRoutes } from './task.routing';
import { TaskComponent } from './tasklist/task.component';
import { TaskService } from '../_services/task.service';
import { TaskCreateComponent } from './taskcreate/task-create.component';
import { ProjectMemberService } from '../_services/project-member.service';
import { AssignmentComponent } from './assignments/assignment.component';
import { DataTablesModule } from 'angular-datatables';
import { TaskDetailsComponent } from './taskdetails/task-details.component';
import { ImageShowDialogComponent } from './imagedialog/image.dialog';
import { WizardCreateTaskComponent } from './wizardtask/wizard.create.task.component';


@NgModule({
    imports: [
        RouterModule.forChild(TaskRoutes),
        CommonModule,
        FormsModule,
        MaterialModule,
        MessagesModule,
        DataTablesModule
    ],
    declarations: [
        TaskComponent,
        TaskCreateComponent,
        TaskDetailsComponent,
        AssignmentComponent,
        ImageShowDialogComponent,
        WizardCreateTaskComponent
    ],
    providers: [
        TaskService,
        ProjectMemberService
    ],
    entryComponents: [
        ImageShowDialogComponent
    ]
})

export class TaskModule {}
