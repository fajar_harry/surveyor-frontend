import { Routes } from '@angular/router';
import { TaskComponent } from './tasklist/task.component';
import { TaskCreateComponent } from './taskcreate/task-create.component';
import { AssignmentComponent } from './assignments/assignment.component';
import { TaskDetailsComponent } from './taskdetails/task-details.component';
import { WizardCreateTaskComponent } from './wizardtask/wizard.create.task.component';


export const TaskRoutes: Routes = [
    {
      path: '',
      children: [ 
          { path: '', component: TaskComponent},
          { path: 'create', component: WizardCreateTaskComponent},
          { path: 'details', component: TaskDetailsComponent},
          { path: 'assign', component: AssignmentComponent}
      ]
    }
];
