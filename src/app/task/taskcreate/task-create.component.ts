import { Component, OnInit, OnDestroy, AfterViewInit, ElementRef, Renderer2, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { TaskService } from "../../_services/task.service";
import { Task } from "../../_models/task";
import { AppSettings } from "../../_services/app-settings";

declare const $: any;
// declare var google;

@Component({
  templateUrl: './task-create.component.html'
})
export class TaskCreateComponent implements OnInit {
  center_lat: number = -6.312218;
  center_lng: number = 106.6924753;

  task: Task = new Task();
  urlCache = new Map<string, SafeResourceUrl>();

  frameId: string = "ESRI_IFRAME";

  uuid: string;
  loading = false;
  projectId: number;

  constructor(
    private sanitizer: DomSanitizer,
    private taskService: TaskService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() { 
    this.uuid = this.generateUUID();
    this.route
      .queryParams
      .map(params => params['project'])
      .subscribe(id => this.projectId = id);
  }

  onSave(): void {
    this.loading = true;
    //this.task.latitude = this.center_lat;
    //this.task.longitude = this.center_lng;
    this.task.uuid = this.uuid;
    this.task.projectId = this.projectId;
    console.log(this.task.projectId);
    this.taskService.saveTask(this.task)
      .subscribe(result => {
        console.log(result)
        this.router.navigate(['/tasks'], { queryParams: { project: this.projectId } });
      }, (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          console.log('An error occurred:', err.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          let message = `Backend returned code ${err.status}, body was: ${err.error}`;
          this.showNotification(message, 'danger');
          console.log(message);
        }
        this.loading = false;
      });
  }

  onCancel(): void {
    this.router.navigate(['tasks'], { queryParams: { project: this.projectId } });
  }

  getIframeEsriUrl(): SafeResourceUrl {
    let url = this.urlCache.get(this.frameId);
    if (!url) {
      url = this.sanitizer.bypassSecurityTrustResourceUrl(AppSettings.MAP_ENDPOINT + "map.html?key=" + this.uuid);;
      this.urlCache.set(this.frameId, url);
    }
    return url;
  }

  showNotification(alert: string, msgType: string) {
    $.notify({
      icon: 'notifications',
      message: alert
    }, {
        type: msgType,
        timer: 3000,
        placement: {
          from: 'top',
          align: 'right'
        }
      });
  }

  generateUUID(): string { // Public Domain/MIT
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
      d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }
}