import { Component, OnInit, OnDestroy, AfterViewInit, ElementRef, Renderer2, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { MatDialogRef, MatDialog, MatDialogConfig } from "@angular/material";
import { TaskService } from "../../_services/task.service";
import { Task } from "../../_models/task";
import { ImageTask } from "../../_models/image-task";
import { ImageShowDialogComponent } from "../imagedialog/image.dialog";
import { AppSettings } from "../../_services/app-settings";

declare const $: any;
// declare var google;

@Component({
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit, AfterViewInit {
  task: Task = new Task();
  projectId: number;
  taskId: number;
  urlCache = new Map<string, SafeResourceUrl>();

  private divHeight: number;

  private imageShowDialogRef: MatDialogRef<ImageShowDialogComponent>;

  images: ImageTask[] = [];

  frameId: string = "ESRI_IFRAME";

  constructor(
    private dialog: MatDialog,
    private sanitizer: DomSanitizer,
    private taskService: TaskService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        this.taskId = params['task'];
        this.taskService.findById(this.taskId)
          .subscribe(task => this.task = task)

        this.projectId = params['project'];

        this.taskService.getImages(this.taskId)
          .subscribe(imageTasks => {
            this.images = imageTasks;
          }, (err: HttpErrorResponse) => {
            this.images = [];
          });
      });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.divHeight = document.getElementById('main-content').clientHeight - 70;
    });
  }

  onBackClick(): void {
    this.router.navigate(['tasks'], { queryParams: { project: this.projectId } });
  }

  getImageAt(index: number): SafeResourceUrl {
    let base64Img = this.images[index].encoding + "," + this.images[index].image;
    let url: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(base64Img);
    return url;
  }

  openImage(index: number): void {
    const matDialogConfig = new MatDialogConfig();

    this.imageShowDialogRef = this.dialog.open(ImageShowDialogComponent, matDialogConfig);
    this.imageShowDialogRef.componentInstance.theImage = this.getImageAt(index);
  }


  getIframeEsriUrl(): SafeResourceUrl {
    let url = this.urlCache.get(this.frameId);
    if (!url) {
      url = this.sanitizer.bypassSecurityTrustResourceUrl(AppSettings.MAP_ENDPOINT + "task-query.html?task=" + this.taskId);;
      this.urlCache.set(this.frameId, url);
    }
    return url;
  }
}