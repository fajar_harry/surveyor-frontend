import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Task } from '../../_models/task';
import { TaskService } from '../../_services/task.service';
import { AppSettings } from '../../_services/app-settings';

declare const $: any;

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit, AfterViewInit {
  private tasks: Task[];
  private projectId: number;
  private projectName: string;

  private divHeight: number;

  urlCache = new Map<string, SafeResourceUrl>();

  frameId: string = "ESRI_IFRAME";

  constructor(
    private taskService: TaskService,
    private router: Router,
    private route: ActivatedRoute,
    private sanitizer: DomSanitizer
  ){}

  public ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.projectId = params['project'];
      this.projectName = params['name'];
    })
    
    this.taskService.findByProject(this.projectId)
      .subscribe(
      tasks => {
        this.tasks = tasks;
        console.log(tasks);
      });
    
  }

  public ngAfterViewInit() {
    setTimeout(() => {
      this.divHeight = document.getElementById('main-content').clientHeight - 70;
    });
  }

  gotoAssignment(taskId: number): void {
    this.router.navigate(['tasks', 'assign'], { queryParams: { task: taskId, project: this.projectId }})
  }

  createTask(): void{
    this.router.navigate(['tasks', 'create'], { queryParams: { project: this.projectId } });
  }

  onTitleClick(taskId: number) {
    this.router.navigate(['tasks', 'details'], { queryParams: { task: taskId, project: this.projectId } })
  }

  getIframeEsriUrl(): SafeResourceUrl {
    let url = this.urlCache.get(this.frameId);
    if (!url) {
      url = this.sanitizer.bypassSecurityTrustResourceUrl(AppSettings.MAP_ENDPOINT + "query.html?project=" + this.projectId);;
      this.urlCache.set(this.frameId, url);
    }
    return url;
  }

}
