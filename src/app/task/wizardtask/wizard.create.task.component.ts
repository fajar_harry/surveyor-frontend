// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, OnInit, OnDestroy, OnChanges, AfterViewInit, SimpleChanges, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { Task } from '../../_models/task';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { ProjectMember } from '../../_models/project-member';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ProjectMemberService } from '../../_services/project-member.service';
import { TaskService } from '../../_services/task.service';
import { AppSettings } from '../../_services/app-settings';


declare const $: any;

@Component({
    selector: 'app-wizard-create-task',
    templateUrl: 'wizard.create.task.component.html',
    styleUrls: ['./wizard.create.task.component.css']
})

export class WizardCreateTaskComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtOptions: DataTables.Settings = {};

    private sub: any;
    private projectId: number;
    private uuid: string;
    private task = new Task();
    private members: ProjectMember[] = [];
    private dtTrigger: Subject<any> = new Subject();

    private urlCache = new Map<string, SafeResourceUrl>();
    private frameId: string = "ESRI_IFRAME";
    

    constructor(
        private sanitizer: DomSanitizer,
        private router: Router,
        private route: ActivatedRoute,
        private memberService: ProjectMemberService,
        private taskService: TaskService
    ) { 
        this.task.priority="Tidak Ada";
        this.task.dueDate = new Date();
    }

    showNotification(alert: string, msgType: string) {
        $.notify({
            icon: 'notifications',
            message: alert
        }, {
                type: msgType,
                timer: 3000,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });
    }

    generateUUID(): string { // Public Domain/MIT
        var d = new Date().getTime();
        if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
          d += performance.now(); //use high-precision timer if available
        }
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
          var r = (d + Math.random() * 16) % 16 | 0;
          d = Math.floor(d / 16);
          return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    }

    getIframeEsriUrl(): SafeResourceUrl {
        let url = this.urlCache.get(this.frameId);
        if (!url) {
          url = this.sanitizer.bypassSecurityTrustResourceUrl(AppSettings + "map.html?key=" + this.uuid);;
          this.urlCache.set(this.frameId, url);
        }
        return url;
    }

    onNext() {
        let index = $('#hd-tab-index').val();
        
        if (index == 1) {
            this.memberService.findAllByProject(this.projectId, this.uuid)
                .subscribe(event => {
                    if (event.type === HttpEventType.Response) {
                        if (event.body) {
                            this.members = event.body;
                        } else {
                            this.showNotification("Pilih koordinat penugasan sebelum melanjutkan ke pemilihan petugas", "danger");
                            $('.wizard-card').bootstrapWizard('show',1);
                        }
                    }
                });
        }
    }

    onAuto() {
        let i = 0;
        while(i < 10 || i < this.members.length) {
            this.members[i].assigned = true;
            i++;
        }
    }

    onFinish() {
        // validate members
        let assigned: ProjectMember[] = this.members.filter(el => {
            return el.assigned;
        })
        
        if (!assigned && assigned.length <= 0) {
            this.showNotification("Minimal satu surveyor di assign untuk penugasan ini", "danger");
            return;
        }

        this.task.members = assigned;
        this.task.projectId = this.projectId;
        this.task.uuid = this.uuid;
        this.taskService.saveTaskWizard(this.task)
            .subscribe(event => {
                if (event.type == HttpEventType.Response) {
                    if (!event.body) {
                        this.showNotification("Lengkapi coordinat penugasan", "danger");
                        $('.wizard-card').bootstrapWizard('show',1);
                    } else
                        this.router.navigate(['tasks'], { queryParams: { project: this.projectId } })
                }
            }, error => {
                if (error === 'Token_Expired') this.router.navigate(['/login']);
                else this.showNotification(error, 'danger');
            });
    }

    

    ngOnInit() {
        this.uuid = this.generateUUID();
        this.route
        .queryParams
        .map(params => params['project'])
        .subscribe(id => this.projectId = id);

        this.dtOptions = {
            pagingType: 'full_numbers',
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
            language: {
              search: '_INPUT_',
              searchPlaceholder: 'Search records',
            }
          };

        // Code for the Validator
        const $validator = $('.wizard-card form').validate({
            rules: {
                taskName: {
                    required: true,
                    minlength: 3
                }, 
                taskDescription: {
                    required: true,
                    minlength: 3
                },
                taskLocation: {
                    required: true,
                    minlength: 3
                }
            },

            errorPlacement: function (error: any, element: any) {
                $(element).parent('div').addClass('has-error');
            }
        });

        // Wizard Initialization
        $('.wizard-card').bootstrapWizard({
            'tabClass': 'nav nav-pills',
            'nextSelector': '.btn-next',
            'previousSelector': '.btn-previous',

            onNext: function (tab, navigation, index) {
                var $valid = $('.wizard-card form').valid();
                if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                } 

                return true;
            },

            onInit: function (tab: any, navigation: any, index: any) {

                // check number of tabs and fill the entire row
                let $total = navigation.find('li').length;
                let $wizard = navigation.closest('.wizard-card');

                let $first_li = navigation.find('li:first-child a').html();
                let $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
                $('.wizard-card .wizard-navigation').append($moving_div);

                $total = $wizard.find('.nav li').length;
                let $li_width = 100 / $total;

                let total_steps = $wizard.find('.nav li').length;
                let move_distance = $wizard.width() / total_steps;
                let index_temp = index;
                let vertical_level = 0;

                let mobile_device = $(document).width() < 600 && $total > 3;

                if (mobile_device) {
                    move_distance = $wizard.width() / 2;
                    index_temp = index % 2;
                    $li_width = 50;
                }

                $wizard.find('.nav li').css('width', $li_width + '%');

                let step_width = move_distance;
                move_distance = move_distance * index_temp;

                let $current = index + 1;

                if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
                    move_distance -= 8;
                } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
                    move_distance += 8;
                }

                if (mobile_device) {
                    let x: any = index / 2;
                    vertical_level = parseInt(x);
                    vertical_level = vertical_level * 38;
                }

                $wizard.find('.moving-tab').css('width', step_width);
                $('.moving-tab').css({
                    'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
                    'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

                });
                $('.moving-tab').css('transition', 'transform 0s');
            },

            onTabClick: function (tab: any, navigation: any, index: any) {

                const $valid = $('.wizard-card form').valid();

                if (!$valid) {
                    return false;
                } else {
                    return true;
                }
            },

            onTabShow: function (tab: any, navigation: any, index: any) {
                // set title
                let titles: string[] = ["Penugasan", "Lokasi", "Petugas"];
                $(".wizard-title").html(titles[index]);
                $('#hd-tab-index').val(index);

                let $total = navigation.find('li').length;
                let $current = index + 1;

                const $wizard = navigation.closest('.wizard-card');

                // If it's the last tab then hide the last button and show the finish instead
                if ($current >= $total) {
                    $($wizard).find('.btn-next').hide();
                    $($wizard).find('.btn-finish').show();
                    $($wizard).find('.btn-auto').show();
                } else {
                    $($wizard).find('.btn-next').show();
                    $($wizard).find('.btn-finish').hide();
                    $($wizard).find('.btn-auto').hide();
                }

                const button_text = navigation.find('li:nth-child(' + $current + ') a').html();

                setTimeout(function () {
                    $('.moving-tab').text(button_text);
                }, 150);

                const checkbox = $('.footer-checkbox');

                if (index !== 0) {
                    $(checkbox).css({
                        'opacity': '0',
                        'visibility': 'hidden',
                        'position': 'absolute'
                    });
                } else {
                    $(checkbox).css({
                        'opacity': '1',
                        'visibility': 'visible'
                    });
                }
                $total = $wizard.find('.nav li').length;
                let $li_width = 100 / $total;

                let total_steps = $wizard.find('.nav li').length;
                let move_distance = $wizard.width() / total_steps;
                let index_temp = index;
                let vertical_level = 0;

                let mobile_device = $(document).width() < 600 && $total > 3;

                if (mobile_device) {
                    move_distance = $wizard.width() / 2;
                    index_temp = index % 2;
                    $li_width = 50;
                }

                $wizard.find('.nav li').css('width', $li_width + '%');

                let step_width = move_distance;
                move_distance = move_distance * index_temp;

                $current = index + 1;

                if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
                    move_distance -= 8;
                } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
                    move_distance += 8;
                }

                if (mobile_device) {
                    let x: any = index / 2;
                    vertical_level = parseInt(x);
                    vertical_level = vertical_level * 38;
                }

                $wizard.find('.moving-tab').css('width', step_width);
                $('.moving-tab').css({
                    'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
                    'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

                });
            }
        });

        $('.set-full-height').css('height', 'auto');
    }

    ngOnDestroy() {}

    
    ngAfterViewInit() {
        $('.wizard-card').each(function () {

            const $wizard = $(this);
            const index = $wizard.bootstrapWizard('currentIndex');
            let $total = $wizard.find('.nav li').length;
            let $li_width = 100 / $total;

            let total_steps = $wizard.find('.nav li').length;
            let move_distance = $wizard.width() / total_steps;
            let index_temp = index;
            let vertical_level = 0;

            let mobile_device = $(document).width() < 600 && $total > 3;

            if (mobile_device) {
                move_distance = $wizard.width() / 2;
                index_temp = index % 2;
                $li_width = 50;
            }

            $wizard.find('.nav li').css('width', $li_width + '%');

            let step_width = move_distance;
            move_distance = move_distance * index_temp;

            let $current = index + 1;

            if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
                move_distance -= 8;
            } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
                move_distance += 8;
            }

            if (mobile_device) {
                let x: any = index / 2;
                vertical_level = parseInt(x);
                vertical_level = vertical_level * 38;
            }

            $wizard.find('.moving-tab').css('width', step_width);
            $('.moving-tab').css({
                'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
                'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

            });

            $('.moving-tab').css({
                'transition': 'transform 0s'
            });
        });
    }
}
