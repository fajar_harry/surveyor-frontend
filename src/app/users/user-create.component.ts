import { Component, OnInit, OnDestroy, AfterViewInit, ElementRef, Renderer2, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpEventType, HttpResponse } from '@angular/common/http';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { ApplicationUser } from "../_models/application-user";
import { Role } from "../_models/role";
import { ApplicationUserService } from "../_services/application-user.service";


declare const $: any;
interface FileReaderEventTarget extends EventTarget {
  result: string;
}

interface FileReaderEvent extends Event {
  target: FileReaderEventTarget;
  getMessage(): string;
}

@Component({
  templateUrl: './user-create.component.html',
  providers: [ApplicationUserService]
})
export class UserCreateComponent implements OnInit, OnDestroy, AfterViewInit {

  private nativeElement: any;

  userForm: FormGroup;
  loading = false;
  model: ApplicationUser;
  id: number;
  sub: any;
  formTitle: string;
  // photoFile: File;

  roles: Role[] = [
    { "roleId": 1, "roleName": 'Administrator' },
    { "roleId": 2, "roleName": "Dispatcher" },
    { "roleId": 3, "roleName": "Surveyor" }
  ];

  constructor(
    private element: ElementRef,
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userService: ApplicationUserService,
    private dialog: MatDialog) {
    this.nativeElement = element.nativeElement;
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  ngAfterViewInit(): void {
    if (this.id)
      this.renderer.selectRootElement('#name').focus();
  }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      username: ['', Validators.required],
      fullname: ['', Validators.required],
      email: ['', Validators.required],
      roleId: []
    });

    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id) { //edit form
      this.formTitle = 'Edit Application User Form';
      this.userService.findById(this.id)
      .subscribe(
        event => {
          if (event.type === HttpEventType.Response) {
            let user = event.body;
            this.id = user.userId;
            this.userForm.patchValue({
              id: user.userId,
              username: user.username,
              fullname: user.fullname,
              email: user.email,
              roleId: user.role.roleId
            });
            $('#photoPreview').attr('src', `${user.encoding},${user.image}`).fadeIn('slow');   
            } 
        }, 
        error => {
          if (error === 'Token_Expired') this.router.navigate(['/login']);
          else this.showNotification(error, 'danger');
        });
    } else this.formTitle = "New Application User Form";

    $('#photo-picture').change(function () {
      const input = $(this);

      if (input[0].files && input[0].files[0]) {
        const reader = new FileReader();

        reader.onload = function (e: FileReaderEvent) {
          $('#photoPreview').attr('src', e.target.result).fadeIn('slow');
        };
        reader.readAsDataURL(input[0].files[0]);
      }
    });
  }

  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field).valid && form.get(field).touched;
  }

  displayFieldCss(form: FormGroup, field: string) {
    return {
      'has-error': this.isFieldValid(form, field),
      'has-feedback': this.isFieldValid(form, field)
    };
  }

  onSave(): void {
    this.loading = true;
    if (this.userForm.valid) {
      this.model = this.userForm.value;
      this.model.userId = this.id;
      //let photoUri: string = $('#photoPreview').attr('src');
      //this.model.photo = this.dataURItoBlob(photoUri);
      if (this.id) {
        this.model.image = $('#photoPreview').attr('src');
        this.userService.updateApplicationUser(this.userForm.value)
          .subscribe(result => {
            this.router.navigate(['users']);
          }, (err: HttpErrorResponse) => {
            console.log("An error occured");
          })
      } else {
        console.log(this.model);
        this.model.image = $('#photoPreview').attr('src');
        this.userService.saveApplicationUser(this.model)
          .subscribe(result => {
            this.router.navigate(['users']);            
          }, (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              let message = `Backend returned code ${err.status}, body was: ${err.error}`;
              this.showNotification('top', 'right', message);
              console.log(message);
            }
            this.loading = false;
          });
      }
    } else {
      this.validateAllFormFields(this.userForm);
      this.loading = false;
    }
  }

  onCancel(): void {
    this.router.navigate(['/users']);
  }

  editMode(): boolean {
    return this.id != (null || undefined);
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      console.log(field);
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  showNotification(from: any, align: any, alert?: string) {
    const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];

    const color = Math.floor((Math.random() * 6) + 1);

    $.notify({
      icon: 'notifications',
      message: alert
    }, {
        type: type[color],
        timer: 3000,
        placement: {
          from: from,
          align: align
        }
      });
  }

  dataURItoFile(dataURI): File {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    let theBlob = new Blob([ia], { type: mimeString });
    return this.blobToFile(theBlob, 'avatar.png');
  }

  blobToFile(theBlob, fileName?): File {
    //A Blob() is almost a File() - it's just missing the two properties below which we will add
    if (fileName) {
      theBlob.lastModifiedDate = new Date();
      theBlob.name = fileName;
    }
    return theBlob;
  }
}