import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';

import { Subject } from 'rxjs/Subject';

import { ApplicationUserService } from "../_services/application-user.service";
import { ApplicationUser } from '../_models/application-user';
import { HttpEventType } from '@angular/common/http';

declare const $: any;

@Component({
  selector: 'app-appuser-list',
  templateUrl: './user-list.component.html',
  providers: [ApplicationUserService]
})
export class UserListComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  applicationUsers: ApplicationUser[] = [];

  headerRow: string[] = ['ID', 'Username', 'Full Name', 'Email', 'Role', 'Active Status', 'Lock Status', 'Last Login', 'Action'];

  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject();

  constructor(private router: Router,
              private userService: ApplicationUserService) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
      language: {
      search: '_INPUT_',
      searchPlaceholder: 'Search records',
      }
    };

    this.userService.findAll()
    .subscribe(
      event => {
        if (event.type === HttpEventType.Response) {
          this.applicationUsers = event.body;
          this.dtTrigger.next();
        } 
      }, 
      error => {
        if (error === 'Token_Expired') this.router.navigate(['/login']);
        else this.showNotification(error, 'danger');
      });
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.userService.findAll()
      .subscribe(
        event => {
          if (event.type === HttpEventType.Response) {
            this.applicationUsers = event.body;
            this.dtTrigger.next();
          } 
        }, 
        error => {
          if (error === 'Token_Expired') this.router.navigate(['/login']);
          else this.showNotification(error, 'danger');
        });
    });
  }

  newUser(): void {
    this.router.navigate(['users/create']);
  }

  onDelete(id:number): void {
    this.userService.deleteApplicationUserById(id)
      .subscribe(
        event => {
          if (event.type === HttpEventType.Response) {
            if (event.body === true) 
            this.rerender();
          } 
        }, 
        error => {
          if (error === 'Token_Expired') this.router.navigate(['/login']);
          else {
            this.showNotification('top', 'right', error);
          }
        });
  }

  onEdit(id: number): void {
    this.router.navigate(['users/edit', id]);
  }

  onDeactivate(id: number): void {
    this.changeUserActiveStatus(id, 0);
  }

  onActivate(id: number): void {
    this.changeUserActiveStatus(id, 1);
  }

  onUnlock(id: number): void {
    this.changeUserLockStatus(id, 0);
  }

  onLock(id: number): void {
    this.changeUserLockStatus(id, 1);
  }

  onResetPassword(id: number): void {
    this.userService.resetUserPassword(id)
    .subscribe(
      event => {
        if (event.type === HttpEventType.Response) {
          let status = event.body === true ? 'success' : 'failed';
          let message = `reset password ${status}`;
          this.showNotification('top', 'right', message);
        } 
      }, 
      error => {
        if (error === 'Token_Expired') this.router.navigate(['/login']);
        else this.showNotification(error, 'danger');
      });
  }

  private changeUserActiveStatus(userId: number, status: number) {
    this.userService.changeUserActiveStatus(userId, status)
    .subscribe(result => {
      var message = status == 1 ? 'User activation' : 'User deactivation';
      if (result)
        this.showNotification('top', 'right', `${message} success`);
      else
        this.showNotification('top', 'right', `${message} failed`);
    });

    let user:ApplicationUser = this.applicationUsers.find(user => user.userId === userId);
    user.enable = status;
  }

  private changeUserLockStatus(userId: number, status: number) {
    this.userService.changeUserLockStatus(userId, status)
    .subscribe(result => {
      var message = status == 1 ? 'User unlocked' : 'User locked';
      if (result)
        this.showNotification('top', 'right', `${message} success`);
      else
      this.showNotification('top', 'right', `${message} failed`);
    });

    let user:ApplicationUser = this.applicationUsers.find(user => user.userId === userId);
    user.locked = status;
  }

  showNotification(from: any, align: any, alert?: string) {
    const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];

    const color = Math.floor((Math.random() * 6) + 1);

    $.notify({
        icon: 'notifications',
        message: alert
    }, {
        type: type[color],
        timer: 3000,
        placement: {
            from: from,
            align: align
        }
    });
}
}