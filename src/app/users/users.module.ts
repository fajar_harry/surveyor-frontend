import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { DataTablesModule } from 'angular-datatables';

import { UserListComponent } from './user-list.component';
import { UsersRoutes } from './users.routing';
import { UserCreateComponent } from './user-create.component';
import { MessagesModule } from 'app/messages/messages.module';

@NgModule({
    imports: [
        RouterModule.forChild(UsersRoutes),
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        DataTablesModule,
        MessagesModule
    ],
    declarations: [
        UserListComponent,
        UserCreateComponent
    ]
})

export class UsersModule {}
