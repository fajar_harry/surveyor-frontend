import { Routes } from '@angular/router';

import { UserListComponent } from './user-list.component';
import { UserCreateComponent } from './user-create.component';

export const UsersRoutes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: UserListComponent }, 
            { path: 'create', component: UserCreateComponent },
            { path: 'edit/:id', component: UserCreateComponent },
        ]
    }
];
