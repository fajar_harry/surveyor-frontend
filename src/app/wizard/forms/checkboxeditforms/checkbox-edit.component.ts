import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Question } from "../../../_models/question";
import { QuestionItem } from "app/_models/questionItem";

declare const $: any;

@Component({
  selector: 'app-form-checkboxedit',
  templateUrl: './checkbox-edit.component.html'
})
export class CheckboxEditComponent implements OnInit {

  @Input() index: number;
  @Input() question: Question;
  @Output() onQuestionCancel = new EventEmitter<number>();
  @Output() onQuestionSave = new EventEmitter<Question>();

  constructor() { }

  ngOnInit(): void { }

  onAddOptions(): void {
    let options = this.question.questionItems;
    let current: number = options.length;
    if (options[current - 1] && options[current - 1].item === 'Others')
      this.showNotification("Tidak bisa menambah options baru setelah options 'Others'", 'danger');
    else {
      let item: QuestionItem = new QuestionItem();
      item.orderNumber = current +  1;
      item.item = `Options ${current + 1}`;
      options.push(item);
    }
  }

  onDeleteOptions(index: number): void {
    this.question.questionItems.splice(index, 1);
  }

  onAddOthers(): void {
    let options = this.question.questionItems;
    let current: number = options.length;
    if (options[current - 1] && options[current - 1].item === 'Others')
      this.showNotification("Tidak bisa menambah options baru setelah options 'Others'", 'danger');
    else {
      let item: QuestionItem = new QuestionItem();
      item.orderNumber = current +  1;
      item.item = `Others`;
      options.push(item);
    }
  }

  isOthers(value: string): boolean {
    return value === 'Others';
  }

  placeholders(index: number): string {
    return this.question.questionItems[index].item;
  }

  cancel(): void {
    this.onQuestionCancel.emit(this.index);
  }

  save(): void {
    this.onQuestionSave.emit(this.question);
  }

  showNotification(alert: string, msgType: string) {
    $.notify({
      icon: 'notifications',
      message: alert
    }, {
        type: msgType,
        timer: 3000,
        placement: {
          from: 'top',
          align: 'right'
        }
      });
  }

  trackByIndex(index: number, value: number) {
    return index;
  }

}