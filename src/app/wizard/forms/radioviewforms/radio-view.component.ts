import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Question } from "../../../_models/question";


@Component({
  selector: 'app-form-radioview',
  templateUrl: './radio-view.component.html'
})
export class RadioViewComponent implements OnInit {

  @Input() question: Question;
  @Output() onDeleteQuestion = new EventEmitter<Question>();
  @Output() onEditQuestion = new EventEmitter<Question>();

  constructor(){}

  ngOnInit(): void {}

  delete() {
    this.onDeleteQuestion.emit(this.question);
  }

  edit() {
    this.onEditQuestion.emit(this.question);
  }
}