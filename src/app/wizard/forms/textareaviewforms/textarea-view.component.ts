import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Question } from "../../../_models/question";


@Component({
  selector: 'app-form-textareaview',
  templateUrl: './textarea-view.component.html'
})
export class TextAreaViewComponent implements OnInit {

  @Input() question: Question;
  @Output() onDeleteQuestion = new EventEmitter<Question>();
  @Output() onEditQuestion = new EventEmitter<Question>();

  constructor(){}

  ngOnInit(): void {}

  delete() {
    this.onDeleteQuestion.emit(this.question);
  }

  edit() {
    this.onEditQuestion.emit(this.question);
  }
}