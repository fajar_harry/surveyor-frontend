import { Component, OnInit, Input, Output, EventEmitter} from "@angular/core";
import { Question } from "../../../_models/question";


@Component({
  selector: 'app-form-textedit',
  templateUrl: './text-edit.component.html'
})
export class TextEditComponent implements OnInit {

  @Input() index: number;
  @Input() question: Question;
  @Output() onQuestionCancel = new EventEmitter<number>();
  @Output() onQuestionSave = new EventEmitter<Question>();

  constructor(){}

  ngOnInit(): void {}

  cancel(): void {
    this.onQuestionCancel.emit(this.index);
  }

  save(): void {
    this.onQuestionSave.emit(this.question);
  }

}