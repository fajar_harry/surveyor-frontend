import { Component, Input } from "@angular/core";
import { Subject } from 'rxjs/Subject';
import { MatDialogRef } from "@angular/material";
import { ApplicationUser } from "../../_models/application-user";
import { AssignmentService } from "../../_services/assignment.service";
import { ApplicationUserService } from "../../_services/application-user.service";
import { HttpEventType } from "@angular/common/http";
import { Router } from "@angular/router";

declare const $: any;

@Component({
  templateUrl: './officer-lookup.dialog.html'
})
export class OfficerLookupDialogComponent {

  dtOptions: DataTables.Settings = {};
  users: ApplicationUser[] = [];
  selectedUsers: ApplicationUser[];

  isDataChanged: boolean = false;

  headerRow: string[] = ['', 'ID', 'Name', 'Email', 'Dispatcher', 'Petugas Lapangan'];

  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject();

  selectedData: ApplicationUser[] = [];

  noSelectedMessage: string;

  constructor(
    private router: Router,
    private userService: ApplicationUserService,
    private dialogRef: MatDialogRef<OfficerLookupDialogComponent>
  ) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
      language: {
        search: '_INPUT_',
        searchPlaceholder: 'Search records',
      }
    };

    this.userService.findAllSimple()
      .subscribe(
        event => {
          if (event.type === HttpEventType.Response) {
            let users = event.body;
            this.users = users;
            this.users.forEach(el => {
              let exists: ApplicationUser = this.selectedUsers.find(sel => sel.fullname === el.fullname);
              if (exists) {
                el.selected = true;
                el.surveyor = exists.surveyor;
                el.dispatcher = exists.dispatcher;
                this.selectedData.push(el);
              }
            })
            this.dtTrigger.next();
          } 
        }, 
        error => {
          if (error === 'Token_Expired') this.router.navigate(['/login']);
          else this.showNotification(error, 'danger');
        });
  }

  showNotification(alert: string, msgType: string) {
    $.notify({
      icon: 'notifications',
      message: alert
    }, {
        type: msgType,
        timer: 3000,
        placement: {
          from: 'top',
          align: 'right'
        }
      });
  }

  onDialogClose(): void {
    this.dialogRef.close();
  }

  onDialogApprove(): void {
    if (this.isDataChanged || (this.selectedData && this.selectedData.length > 0)) {
      if (this.selectedData.some(el => !el.dispatcher && !el.surveyor)) {
        this.noSelectedMessage = "Terdapat member yang dipilih belum mempunyai peran";
        return;
      }
      this.dialogRef.close(this.selectedData);
    } else {
      this.noSelectedMessage = 'Silahkan pilih satu atau lebih members';
    }
  }

  onRowSelected(user: ApplicationUser, element: HTMLInputElement): void {
    if (element.checked) {
      this.selectedData.push(user);
      user.selected = true;
    } else {
      let index = this.selectedData.indexOf(user);
      this.selectedData.splice(index, 1);
      user.selected = false;
    }

    this.isDataChanged = this.selectedData.length > 0 ? true : false;
  }

  checkChoice(user: ApplicationUser): boolean {
    return !user.selected;
  }

  checkDispatcher(user: ApplicationUser): boolean {
    if (!user.selected) return false;
    else return user.dispatcher;
  }

  checkSurveyor(user: ApplicationUser): boolean {
    if (!user.selected) return false;
    else return user.surveyor;
  }

  onDispatcher(element: HTMLInputElement, user: ApplicationUser) {
    user.dispatcher = element.checked;
  }

  onSurveyor(element: HTMLInputElement, user: ApplicationUser) {
    user.surveyor = element.checked;
  }
}