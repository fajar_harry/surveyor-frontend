import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { DataTablesModule } from 'angular-datatables';
import { PipesModule } from '../_pipe/pipes.module';
import { WizardRoutes } from './wizard.routing';
import { ApplicationUserService } from '../_services/application-user.service';
import { ProjectService } from '../_services/project.service';
import { WizardComponent } from './wizardmain/wizard.component';
import { OfficerLookupDialogComponent } from './userdialog/officer-lookup.dialog';
import { TextEditComponent } from './forms/texteditforms/text-edit.component';
import { TextViewComponent } from './forms/textviewforms/text-view.component';
import { TextAreaViewComponent } from './forms/textareaviewforms/textarea-view.component';
import { RadioEditComponent } from './forms/radioeditforms/radio-edit.component';
import { RadioViewComponent } from './forms/radioviewforms/radio-view.component';
import { CheckboxEditComponent } from './forms/checkboxeditforms/checkbox-edit.component';
import { CheckboxViewComponent } from './forms/checkboxviewforms/checkbox-view.component';
import { FieldErrorDisplayComponent } from '../messages/field-error-display.component';
import { MessagesModule } from '../messages/messages.module';


@NgModule({
    imports: [
        RouterModule.forChild(WizardRoutes),
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        DataTablesModule,
        PipesModule,
        MessagesModule
    ],
    declarations: [
        WizardComponent,
        OfficerLookupDialogComponent,
        TextEditComponent,
        TextViewComponent,
        TextAreaViewComponent,
        RadioEditComponent,
        RadioViewComponent,
        CheckboxEditComponent,
        CheckboxViewComponent
    ],
    providers: [
        ApplicationUserService,
        ProjectService
    ], entryComponents: [
        OfficerLookupDialogComponent
    ]
})

export class WizardModule {}
