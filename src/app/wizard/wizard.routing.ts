import { Routes } from '@angular/router';
import { WizardComponent } from './wizardmain/wizard.component';


export const WizardRoutes: Routes = [
    {
      path: '',
      children: [ 
          { path: '', component: WizardComponent},
          { path: 'conf/:projectid', component: WizardComponent },
      ]
    }
];
