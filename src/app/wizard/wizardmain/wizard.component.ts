// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, OnInit, OnDestroy, OnChanges, AfterViewInit, SimpleChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { ApplicationUserService } from '../../_services/application-user.service';
import { ApplicationUser } from '../../_models/application-user';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { OfficerLookupDialogComponent } from '../userdialog/officer-lookup.dialog';
import { Question } from '../../_models/question';
import { Project } from '../../_models/project';
import { Task } from '../../_models/task';
import { Assignment } from '../../_models/assignment';
import { ProjectMember } from '../../_models/project-member';
import { ProjectService } from '../../_services/project.service';


declare const $: any;
interface FileReaderEventTarget extends EventTarget {
    result: string;
}

interface FileReaderEvent extends Event {
    target: FileReaderEventTarget;
    getMessage(): string;
}
@Component({
    selector: 'app-wizard-cmp',
    templateUrl: 'wizard.component.html'
})

export class WizardComponent implements OnInit, OnDestroy, OnChanges, AfterViewInit {

    private members: ApplicationUser[] = [];
    private questions: Question[] = [];
    private project: Project = new Project();

    private officerLookupDialogRef: MatDialogRef<OfficerLookupDialogComponent>;

    private sub: any;
    private projectId: number;

    constructor(
        private dialog: MatDialog,
        private projectService: ProjectService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    showNotification(alert: string, msgType: string) {
        $.notify({
            icon: 'notifications',
            message: alert
        }, {
                type: msgType,
                timer: 3000,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });
    }

    onFinish() {
        this.project.users = this.members;
        this.project.questionForms = this.questions;
        this.project.icon = $('#photoPreview').attr('src');
        this.projectService.saveProject(this.project)
            .subscribe(result => {
                this.router.navigate(['projects']);
            }, (err: HttpErrorResponse) => {
                if (err.error instanceof Error) {
                    // A client-side or network error occurred. Handle it accordingly.
                    console.log('An error occurred:', err.error.message);
                } else {
                    // if exception is com.surveyor.backend.exception.UnsupportedEditOperationException
                    // the form is not allow to changed
                    let message = `Backend returned code ${err.status}, body was: ${err.message}`;
                    this.showNotification(message, 'danger');
                }
            });
    }

    onDeleteQuestion(question: Question) {
        this.questions.splice(question.orderNumber, 1);
    }

    onEditQuestion(question: Question) {
        let q: Question = this.questions[question.orderNumber];
        q.mode = 'edit';
    }

    onQuestionCancel(index: number): void {
        let q = this.questions[index];
        if (q.mode === 'insert')
            this.questions.splice(index, 1);
        else
            q.mode = 'view';
    }

    onQuestionSave(question: Question): void {
        let index = question.orderNumber;
        question.mode = 'view';
        this.questions[index] = question;
    }

    onTextQuestionAdd(type: string): void {
        let question: Question = new Question();
        question.type = type;
        question.orderNumber = this.questions.length;
        question.mode = 'insert';
        this.questions.push(question);
    }

    onMemberAdd(): void {
        const matDialogConfig = new MatDialogConfig();
        matDialogConfig.width = "860px";
        matDialogConfig.height = "600px";
        this.officerLookupDialogRef = this.dialog.open(OfficerLookupDialogComponent, matDialogConfig);
        this.officerLookupDialogRef.componentInstance.selectedUsers = this.members;
        this.officerLookupDialogRef.afterClosed()
            .subscribe(users => {
                this.members = [];
                this.members.push.apply(this.members, users);
            });
    }

    onMemberRemove(user: ApplicationUser) {
        let assigned: boolean = false;
        // cek sudah ada assignment
        let tasks: Task[] = this.project.tasks;
        if (tasks && tasks.length > 0) {
            tasks.forEach(task => {
                if (task.status != 0) {
                    let assignments: Assignment[] = task.assignment;
                    assignments.forEach(ass => {
                        let member: ProjectMember = ass.member;
                        if (member.user.userId == user.userId) {
                            this.showNotification(`Member ini sudah mempunyai penugasan`, 'danger');
                            assigned = true;
                        }
                    });
                }
            });
        }

        if (!assigned) {
            let index: number = this.members.findIndex(el => el.fullname === user.fullname);
            this.members.splice(index, 1);
        }
    }

    delete(): void {
        this.projectService.deleteProjectById(this.project.projectId)
            .subscribe(
            event => {
                if (event.type === HttpEventType.Response) {
                    if (event.status === 200) {
                        this.router.navigate(['projects']);
                    } else if (event.status === 202) 
                        this.showNotification('Kategori ini tidak bisa dihapus karena sudah mempunyai penugasan', 'danger');
                    else
                        this.showNotification("Terjadi kesalahan komunikasi dengan backend server, hubungi administrator", 'danger');
                }
            })
    }

    ngOnInit() {

        // Code for the Validator
        const $validator = $('.wizard-card form').validate({
            rules: {
                projectName: {
                    required: true,
                    minlength: 3
                }
            },

            errorPlacement: function (error: any, element: any) {
                $(element).parent('div').addClass('has-error');
            }
        });

        // Wizard Initialization
        $('.wizard-card').bootstrapWizard({
            'tabClass': 'nav nav-pills',
            'nextSelector': '.btn-next',
            'previousSelector': '.btn-previous',

            onNext: function (tab, navigation, index) {

                var $valid = $('.wizard-card form').valid();
                if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                }
            },

            onInit: function (tab: any, navigation: any, index: any) {

                // check number of tabs and fill the entire row
                let $total = navigation.find('li').length;
                let $wizard = navigation.closest('.wizard-card');

                let $first_li = navigation.find('li:first-child a').html();
                let $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
                $('.wizard-card .wizard-navigation').append($moving_div);

                $total = $wizard.find('.nav li').length;
                let $li_width = 100 / $total;

                let total_steps = $wizard.find('.nav li').length;
                let move_distance = $wizard.width() / total_steps;
                let index_temp = index;
                let vertical_level = 0;

                let mobile_device = $(document).width() < 600 && $total > 3;

                if (mobile_device) {
                    move_distance = $wizard.width() / 2;
                    index_temp = index % 2;
                    $li_width = 50;
                }

                $wizard.find('.nav li').css('width', $li_width + '%');

                let step_width = move_distance;
                move_distance = move_distance * index_temp;

                let $current = index + 1;

                if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
                    move_distance -= 8;
                } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
                    move_distance += 8;
                }

                if (mobile_device) {
                    let x: any = index / 2;
                    vertical_level = parseInt(x);
                    vertical_level = vertical_level * 38;
                }

                $wizard.find('.moving-tab').css('width', step_width);
                $('.moving-tab').css({
                    'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
                    'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

                });
                $('.moving-tab').css('transition', 'transform 0s');
            },

            onTabClick: function (tab: any, navigation: any, index: any) {

                const $valid = $('.wizard-card form').valid();

                if (!$valid) {
                    return false;
                } else {
                    return true;
                }
            },

            onTabShow: function (tab: any, navigation: any, index: any) {
                // set title
                let titles: string[] = ["Buat Proyek", "Pengguna", "Setting Form"];
                $(".wizard-title").html(titles[index]);

                let $total = navigation.find('li').length;
                let $current = index + 1;

                const $wizard = navigation.closest('.wizard-card');

                // If it's the last tab then hide the last button and show the finish instead
                if ($current >= $total) {
                    $($wizard).find('.btn-next').hide();
                    $($wizard).find('.btn-finish').show();
                } else {
                    $($wizard).find('.btn-next').show();
                    $($wizard).find('.btn-finish').hide();
                }

                const button_text = navigation.find('li:nth-child(' + $current + ') a').html();

                setTimeout(function () {
                    $('.moving-tab').text(button_text);
                }, 150);

                const checkbox = $('.footer-checkbox');

                if (index !== 0) {
                    $(checkbox).css({
                        'opacity': '0',
                        'visibility': 'hidden',
                        'position': 'absolute'
                    });
                } else {
                    $(checkbox).css({
                        'opacity': '1',
                        'visibility': 'visible'
                    });
                }
                $total = $wizard.find('.nav li').length;
                let $li_width = 100 / $total;

                let total_steps = $wizard.find('.nav li').length;
                let move_distance = $wizard.width() / total_steps;
                let index_temp = index;
                let vertical_level = 0;

                let mobile_device = $(document).width() < 600 && $total > 3;

                if (mobile_device) {
                    move_distance = $wizard.width() / 2;
                    index_temp = index % 2;
                    $li_width = 50;
                }

                $wizard.find('.nav li').css('width', $li_width + '%');

                let step_width = move_distance;
                move_distance = move_distance * index_temp;

                $current = index + 1;

                if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
                    move_distance -= 8;
                } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
                    move_distance += 8;
                }

                if (mobile_device) {
                    let x: any = index / 2;
                    vertical_level = parseInt(x);
                    vertical_level = vertical_level * 38;
                }

                $wizard.find('.moving-tab').css('width', step_width);
                $('.moving-tab').css({
                    'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
                    'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

                });
            }
        });

        $('.set-full-height').css('height', 'auto');

        // if edit
        this.sub = this.route.params.subscribe(params => {
            if (params['projectid']) {
                this.project = JSON.parse(localStorage.getItem('project'));
                this.projectService.findMembers(params['projectid'])
                .subscribe(
                    event => {
                        if (event.type === HttpEventType.Response) {
                            this.members = event.body;
                        } 
                    }, 
                    error => {
                        if (error === 'Token_Expired') this.router.navigate(['/login']);
                        else this.showNotification(error, 'danger');
                    });
                this.questions = this.project.questionForms;
                this.projectId = this.project.projectId;
                if (this.project.icon)
                    $('#photoPreview').attr('src', `${this.project.encoding},${this.project.icon}`).fadeIn('slow');                    
            }
        });

        $('#photo-picture').change(function () {
            const input = $(this);

            if (input[0].files && input[0].files[0]) {
                const reader = new FileReader();

                reader.onload = function (e: FileReaderEvent) {
                    $('#photoPreview').attr('src', e.target.result).fadeIn('slow');
                };
                reader.readAsDataURL(input[0].files[0]);
            }
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    ngOnChanges(changes: SimpleChanges) {
        const input = $(this);

        if (input[0].files && input[0].files[0]) {
            const reader: any = new FileReader();

            reader.onload = function (e: FileReaderEvent) {
                $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
            };
            reader.readAsDataURL(input[0].files[0]);
        }
    }
    ngAfterViewInit() {
        $('.wizard-card').each(function () {

            const $wizard = $(this);
            const index = $wizard.bootstrapWizard('currentIndex');
            let $total = $wizard.find('.nav li').length;
            let $li_width = 100 / $total;

            let total_steps = $wizard.find('.nav li').length;
            let move_distance = $wizard.width() / total_steps;
            let index_temp = index;
            let vertical_level = 0;

            let mobile_device = $(document).width() < 600 && $total > 3;

            if (mobile_device) {
                move_distance = $wizard.width() / 2;
                index_temp = index % 2;
                $li_width = 50;
            }

            $wizard.find('.nav li').css('width', $li_width + '%');

            let step_width = move_distance;
            move_distance = move_distance * index_temp;

            let $current = index + 1;

            if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
                move_distance -= 8;
            } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
                move_distance += 8;
            }

            if (mobile_device) {
                let x: any = index / 2;
                vertical_level = parseInt(x);
                vertical_level = vertical_level * 38;
            }

            $wizard.find('.moving-tab').css('width', step_width);
            $('.moving-tab').css({
                'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
                'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

            });

            $('.moving-tab').css({
                'transition': 'transform 0s'
            });
        });
    }
}
